# bokeh-apps

This repository contains [Bokeh Applications](https://bokeh.pydata.org/en/latest/docs/user_guide/server.html#building-bokeh-applications)
that are meant to be run on a [Bokeh Server](https://bokeh.pydata.org/en/latest/docs/user_guide/server.html).

Applications shall be created under the `apps` directory.

There is currently one bokeh server deployed in the lab: http://bokeh-apps.cslab.esss.lu.se/

When pushing to master, applications are automatically deployed to this server (the git repository is cloned
and the bokeh server is restarted with the list of applications present under the `apps` directory).

To add or modify an application, please fork this repository and create a Merge Request.
